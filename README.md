# ![[icon]](promo/icon.png) Desert Bloom

**Creator:** kdau#1430
**Workshop code:** `1FVH4`

Gardening, but make it Overwatch! Plant seeds on the desert maps and grow them into beautiful flowers, but look out for damage from the other team. Chill mode also available.

## Gameplay

Players are divided into two teams. Your team has **six digging spots** scattered around your side of the map, along with an **inventory of seeds** to plant. At the spots on your side, you must damage the dirt to "**dig**" it, **plant** the seeds, heal the plants to **grow** them until they bloom, and finally **harvest** the mature flowers.

Progress bars at the top left of the screen track how many plants have been harvested by each team. Whichever team reaches their **target number** first wins. If time runs out before either team has harvested the required number of plants, whichever team has harvested more plants wins. A tie results in a drawn match.

The opposing team will be on the other side of the map gardening at their spots. You can **attack players** on the opposing team (except in chill mode). You can also attack and **damage plants** that have germinated but have not fully grown (except in full chill mode).

## Plant lifecycle

1. **Digging Spot**: An "X" icon marks the spot where you can damage the ground to dig it up for planting. *Some weaker damage abilities may not work for this stage, but quick melee (punching) will work for any hero.*

2. **Hoed Dirt**: A ring icon indicates a hole dug in the ground where you can plant a seed (if you have any remaining). Interact (by default, press "F") to do so.

3. **Sprout**: A flag icon marks a planted seed. It needs extra care to germinate, so heal it until its progress bar is full. Fortunately, it is mostly underground at this stage, so it can't be damaged by the opposing team.

4. **Seedling, Vegetative, Budding**: An up arrow icon signals these three connected stages of plant growth. Keep healing the plant to fill its progress bar again. Watch out, because the plant can now be damaged by the opposing team.

5. **Flower**: At last, your plant has bloomed into a beautiful flower! A spade icon above it confirms that it is ready to harvest. Just interact (by default, press "F") to score one for your team.

## Settings

The host player can change how many plants a team needs to **harvest** in order to win and how many **extra seeds** beyond that minimum a team can have. By default, for example, a team must harvest five plants and is allowed two extra seeds, so they start with 5 + 2 = 7 seeds.

The host can also change the **duration** of the game in minutes. Be sure to leave enough time to grow the number of plants required!

Finally, the host can choose one of three **chill mode** settings. By default ("None"), players can damage both opposing team members and the plants being grown by the opposing team. In "Players" chill mode, players cannot damage other players, but can still damage the opposing team's plants. In "Players and Plants" mode, the plants cannot be damaged either, so everyone experiences tranquility.

## Source

This mode's source code is available in its [GitLab repository](https://gitlab.com/kdau/desert-bloom). It is developed using the [OSTW](https://github.com/ItsDeltin/Overwatch-Script-To-Workshop/) high-level scripting language.
